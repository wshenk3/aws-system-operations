### Lab 8: Deployments with AWS CloudFormation

##### Task 1: Deplay a Cloud Formation Stack

Save task1.yaml
cp ~/Downloads/task1.yaml .
vi task1.yaml

Services >> CloudFormation >> Create New Stack >> Upload a template to Amazon S3
Browse >> Choose File >> Next 

Specify Details 
Stack Name: Lab
Parameters: Default 
Click Next >> Options: Default  >> Click Next 

Capabilities >> Click I ack >> Create

Events Tab >> Resources Tab >> Wait until status says CREATE_COMPLETE

##### Task 2: Add an Amazon S3 Bucket to the Stack 

Sample S3 Snippet 
```
AWSTemplateFormatVersion: 2010-09-09
Resources:
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      AccessControl: PublicRead
      WebsiteConfiguration:
        IndexDocument: index.html
        ErrorDocument: error.html
    DeletionPolicy: Retain
  BucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      PolicyDocument:
        Id: MyPolicy
        Version: 2012-10-17
        Statement:
          - Sid: PublicReadForGetBucketObjects
            Effect: Allow
            Principal: '*'
            Action: 's3:GetObject'
            Resource: !Join 
              - ''
              - - 'arn:aws:s3:::'
                - !Ref S3Bucket
                - /*
      Bucket: !Ref S3Bucket
Outputs:
  WebsiteURL:
    Value: !GetAtt 
      - S3Bucket
      - WebsiteURL
    Description: URL for website hosted on S3
  S3BucketSecureURL:
    Value: !Join 
      - ''
      - - 'https://'
        - !GetAtt 
          - S3Bucket
          - DomainName
    Description: Name of S3 bucket to hold website content
```

Copied original task1.yaml to task1.yaml.orig


Added the below lines to task1.yaml
```
  S3Bucket:
    Type: AWS::S3::Bucket
```

CloudFormation >> Select Lab >> Actions >> Update Stack >> Update template file >> Next
nstanceProfilet2.micro- Key: Name

Specify Details >> Next >> Options >> Next >> Ack >> Update 

##### Task 3: Add an Amazon EC2 Instance to the Stack 

Update the template with the below lines, add to paramters section
```
  AmazonLinuxAMIID:
    Type: AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>
    Default: /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2
```

Update the template to add an EC2 instance with the following properties, everything goes under Resources:

IamInstanceProfile: Refer to InstanceProfile
ImageId: Refer to AmazonLinuxAMIID
InstanceType: t2.micro
SecurityGroupIds: Refer to AppSecurityGroup
```
      SecurityGroupIds:
        - !Ref AppSecurityGroup
```
SubnetId: Refer to PublicSubnet
Tags: Use below
```
      Tags:
        - Key: Name
          Value: App Server
```

Sample EC2 Snippet
```
Type: AWS::EC2::Instance
Properties: 
  Affinity: String
  AvailabilityZone: String
  BlockDeviceMappings: 
    - EC2 Block Device Mapping
  CreditSpecification: CreditSpecification
  DisableApiTermination: Boolean
  EbsOptimized: Boolean
  ElasticGpuSpecifications: [ ElasticGpuSpecification, ... ]
  ElasticInferenceAccelerators: 
    - ElasticInferenceAccelerator
  HostId: String
  IamInstanceProfile: String
  ImageId: String
  InstanceInitiatedShutdownBehavior: String
  InstanceType: String
  Ipv6AddressCount: Integer
  Ipv6Addresses:
    - IPv6 Address Type
  KernelId: String
  KeyName: String
  LaunchTemplate: LaunchTemplateSpecification
  LicenseSpecifications: 
    - LicenseSpecification
  Monitoring: Boolean
  NetworkInterfaces: 
    - EC2 Network Interface
  PlacementGroupName: String
  PrivateIpAddress: String
  RamdiskId: String
  SecurityGroupIds: 
    - String
  SecurityGroups: 
    - String
  SourceDestCheck: Boolean
  SsmAssociations: 
    - SSMAssociation
  SubnetId: String
  Tags: 
    - Resource Tag
  Tenancy: String
  UserData: String
  Volumes: 
    - EC2 MountPoint
  AdditionalInfo: String 
```


Lab Provided solution is in task3.yaml

##### Task 4: Delete the Stack 

Select Lab >> Actions >> Delete >> Yes, Delete


