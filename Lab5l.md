### Lab 5: Managing Storage in AWS

##### Task 1: Creating and Configuring Resources
Services >> S3 >> Create Bucket

Bucket Name: youruniquebucketname1337
Region: default

Create

Services >> IAM >> Roles >> Create Roles

Select Typs of Trusted Entity >> Choose AWS Service >> EC2

Next: Permissions >> Next: Tags >> Next: Review

Role Name: S3BucketAccess

Create Role

Click S3BucketAccess >> Add inline policy >> Create Policy page - click JSON

Delete Existing lines >> Paste the following
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets",
                "s3:HeadBucket",
                "s3:ListBucket"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::YOUR-BUCKET-NAME/*",
                "arn:aws:s3:::YOUR-BUCKET-NAME"
            ]
        }
    ]
}
```

Replace YOUR-BUCKET-NAME with youruniquebucketname1337 (two times)

Click Review Policy >> Create a name for the policy >> Create policy

Services >> EC2 >> Instances >> Processor >> Actions >> Instance Settings >> Attach/Replace IAM Role

Select S3BucketAccess under IAM ROLE

Apply >> Close 

```
chmod 400 ~/Downloads/qwikLABS-L3867-1936144.pem
ssh -i ~/Downloads/qwikLABS-L3867-1936144.pem ec2-user@ec2-34-216-152-212.us-west-2.compute.amazonaws.com
```
```
aws ec2 describe-instances --filter 'Name=tag:Name,Values=Processor'
aws ec2 describe-instances --filter 'Name=tag:Name,Values=Processor' --query 'Reservations[0].Instances[0].BlockDeviceMappings[0].Ebs.{VolumeId:VolumeId}'
```
Results:
```
{
    "VolumeId": "vol-0a5a7292d8e70d5ce"
}
```
```
aws ec2 describe-instances --filters 'Name=tag:Name,Values=Processor' --query 'Reservations[0].Instances[0].InstanceId'
aws ec2 stop-instances --instance-ids i-0739c6c3fef8319b1
aws ec2 wait instance-stopped --instance-id i-0739c6c3fef8319b1
aws ec2 create-snapshot --volume-id vol-0a5a7292d8e70d5ce
aws ec2 wait snapshot-completed --snapshot-id snap-0ac67d076a237a626
aws ec2 start-instances --instance-ids i-0739c6c3fef8319b1
aws ec2 wait instance-running --instance-id i-0739c6c3fef8319b1
```
Schedule Creation of subsequent snapshots
```
echo "* * * * *  aws ec2 create-snapshot --volume-id vol-0a5a7292d8e70d5ce 2>&1 >> /tmp/cronlog" > cronjob
crontab cronjob
aws ec2 describe-snapshots --filters "Name=volume-id,Values=vol-0a5a7292d8e70d5ce"
```
```
cat snapshotter.py
```
```
#!/usr/bin/env python

import boto3

MAX_SNAPSHOTS = 2   # Number of snapshots to keep

# Create the EC2 resource
ec2 = boto3.resource('ec2')

# Get a list of all volumes
volume_iterator = ec2.volumes.all()

# Create a snapshot of each volume
for v in volume_iterator:
  v.create_snapshot()

  # Too many snapshots?
  snapshots = list(v.snapshots.all())
  if len(snapshots) > MAX_SNAPSHOTS:

    # Delete oldest snapshots, but keep MAX_SNAPSHOTS available
    snap_sorted = sorted([(s.id, s.start_time, s) for s in snapshots], key=lambda k: k[1])
    for s in snap_sorted[:-MAX_SNAPSHOTS]:
      print "Deleting snapshot", s[0]
      s[2].delete()

```
```
python snapshotter.py
aws ec2 describe-snapshots --filters "Name=volume-id, Values=vol-0a5a7292d8e70d5ce" --query 'Snapshots[*].SnapshotId'
```

##### Task 3: Challenge: Synchronize Files with Amazon S3

wget https://us-west-2-tcprod.s3.amazonaws.com/courses/ILT-TF-100-SYSOPS/v3.3.2/lab-5-storage-linux/scripts/files.zip
unzip files.zip

aws s3api

* Activate versioning for s3 bucket
* Use single cli command to sync the contents of your unzipped folder with your amazon s3 bucket
* Modify the command so that it deletes a files from amazon s3 when the corresponding file is deleted locally
* Recover the deleted file from amazon S3 using versioning

```
aws s3api put-bucket-versioning help
aws s3api put-bucket-versioning --bucket youruniquebucketname1337 --versioning-configuration MFADelete=Disabled,Status=Enabled
```
```
aws s3 sync files s3://youruniquebucketname1337/ 
```
```
aws s3 sync files s3://youruniquebucketname1337/ --delete
```

```
aws s3api list-object-versions --bucket youruniquebucketname1337
aws s3api get-object --bucket youruniquebucketname1337 --key file1.txt --version-id  l4c6SUQCqkODCzmOyTv.e3FecYwNYjv. recovered_file
^failed, you have to use the non deleted version
aws s3api get-object --bucket youruniquebucketname1337 --key file1.txt --version-id  kEw9XyY8jbMdhMFAMcI_87vwWz6JkCmE recovered_file

cp recovered_file files/file1.txt
aws s3 sync files s3://youruniquebucketname1337/

```
 
