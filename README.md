# AWS System Operations
Anaheim, CA

12 March - 14 March 

Links to Lab Notes

* [Lab1.md](Lab1.md)
* [Lab2l.md](Lab2l.md)
* [Lab3l.md](Lab3l.md)
* [Lab4l.md](Lab4l.md)

* * * 
### AWS Regions
* All Regions except Osaka have at least 2 Availability Zones
* AWS Govcloud West & East
    * Exist for "Compliance", not more security - AWS Guy
* Why pick a Region?
    * Latency / Where are your customers?
    * Compliance (ITAR, GDPR, etc)
    * Availaibility of Services 
    * Costs
* If a certain service is only available in a region that you don't use, you can still use it, you'll be responsible for transit costs

Tue Mar 12 12:59:09 EDT 2019
* * * 
### AWS Edge Locations
* Not fully featured regions
* Usually used for CloudFrount, Content Distrubution
* Route53 - DNS, faster resolution
* Additional Services as well...

Tue Mar 12 13:01:58 EDT 2019
* * * 
### Intro to Core Services
* Compute, Storage, Networking, Security

Tue Mar 12 13:05:34 EDT 2019
* * * 
### Service limits on AWS
* Limit potential excessive charges
* Keep the AWS network healthy
* Soft limits - can be changed on request from AWS tech support
* Hard limits - cannot be changed

Tue Mar 12 13:08:05 EDT 2019
* * * 
### Scenario: Deploying a website
* Gateway, VPC, public subnet, private subnet
* Duplicate in another availability zone
* Add load balancer between AZs
* Monitor and Scale 
    * need statistics
    * b/w, mem usage, i/o, disk usage
    * Auto Scaling Groups
    * Cloud Watch gives you access to stats, can create thresholds for metrics
    * Centralize your logs in S3, S3 - you won't lose your data, lots of 9's
    * Amazon Glacier - rotate your x days old logs here
    * More later
* Backups
    * AWS Storage Gateway - acts as a virtual tape library 
* Hybrid Architecture
    * AWS Direct Connect, as close as you can get to plugging into AWS
    * VPN
    * VPC Peering
* AWS CloudFormation
* Data Replication - depends on the service

Tue Mar 12 13:27:22 EDT 2019
* * * 
### AWS Identity and Access Management (IAM)
* Centrally manage access and authentication of your users to AWS resources
* Offered as a feature of your AWS Account at no charge
* Create users, groups, and roles and apply policies to them ot control their access to AWS resources
* ...
* You don't want to log in as root
* Create IAM user name and password
* If you lose your IAM credentials, you can call AWS support 
* If you lose your root credentials, need notarized document 
* Access keys - cli and sdk
* MFA available
* IAM Users
    * when you first get an IAM user, that user can't do anything
    * you have to grant priv's
    * create user with programmatic and aws mgt console access
* Create IAM policy, attach to users, also groups 
    * Can use visual editor instead of json
    * AWS Managed (out of the box) and Customer Managed (custom) policies exist 
* IAM Precendence
    * Is the action explicitly denied? if yes deny
    * Is the action explicitly allowed? if yes allow
    * Otherwise deny
* IAM Groups cannot be nested "kind of annoying" - AWS guy
* IAM Roles
    * TEMPORARY ACCESS
    * Eliminates the need for static AWS credentials
    * Permissions defined using IAM policies
    * Permissions attached to the role, and assumed by the IAM user
    * You lose other permission when you are attached to a role
    * Create role >> choose entity (services, AWS account, Web id, SAML 2.0 federation) >> Choose Permissions >> Add Tag (Key val pair) >> Role name & desc >> create >> give permissions to user to assume role >> ARN (Amazon Resource Name) for role that the user will assume >> when user assumes a role, user receives temporary token (Secure Token Service STS) i
    * IAM Policy ninja video - link coming later

Tue Mar 12 14:01:00 EDT 2019
* * * 
### Managing Accounts
* Can be leveraged for isolation
* Centralized account management - AWS Organizations
    * Consolidated billing
* Can nest accounts into Organization Units
* "hard to do if you don't start from here" - AWS Guy

Tue Mar 12 14:11:52 EDT 2019
* * * 
### Intro to AWS CLI
* Setup access key, secret key, region name and output format
```
aws configure
```
* CLI Format
```
aws <service> <operations> <paramters> <options>
aws help
aws ec2 help
aws ec2 describe-instances help
```
* Limiting Results 
```
--query
--filter 
```
* Other options: Dry Run - simulates command, 
```
--dry-run
```
* Other options: Output
    * json, text, table
    * default config'd with `aws configure`
```
--output
```
* Common commands: EC2
```
run-instances
describe-instances
create-volume
create-vpc
```
* Commond commands: S3
```
mv
ls
cp
```
* AWS Comprehend
```
aws comprehend detect-sentiment --text "I had a greate time at your restaurant" --language-code=en
```
* AWS Translate
```
aws translate translate-text --text "Hello, welcome to System Operations on AWS" --source-language-code=en --target-language=es
```
* AWS Polly
```
aws polly synthesize-speech --output-format mp3 --voice-id Joanna --text 'hello, my name is joanna. Welcome to system operations on AWS' hello.mp3
```
* AWS Describe Instances
```
aws ec2 describe-instances --query 'Reservations[].Instances[].Tags[?Key==`Name`].Value[]' 
```

Tue Mar 12 14:39:54 EDT 2019
* * * 
### AWS Systems Manager
* Software inventory, OS Patches, System image, OS Management and Configuration
* Automation
    * Create an automation document, automates workflows
* Run Commands
    * Use predefined commands
    * Create your own
    * Choose instances/tags
    * Choose controls/schedules
    * Execute 
* Session Manager
    * Securely connect to instances without opening inbound ports, bastion hosts, or maintaining SSH keys
* Patch Manager
    * Deploy OS and S/W patches automatically across large groups of Amazon EC2 or on-premises instances
* Maintenance Window
    * Schedule window of time to run administrative and maintenance tasks
* State Manager
    * Keeps systems in the configuration you want them to be in
* Parameter Store - Amazon KMS
    * Key Value Storage
    * Great way to save passwords/strings that would otherwise be hardcoded
* Inventory
    * Collections information about instances and the software installed on them
* Insights Dashboard
    * Display of operatioal data for resources
* AWS Tools for Powershell
    * Script operations from powershell
* SDKs
    * JS, Python, PHP, .NET, Ruby, Go, Node.js, C++, Java
* AWS CloudFormation
    * Create, update, and delete a set of resources as a single unit (stack)
    * JSON or YAML
    * Preview changes
    * ...
* AWS OpsWorks
    * "Managed Chef & Puppet Server that interacts with AWS services"
    * Used hand in hand with CloudFormation

Tue Mar 12 15:37:44 EDT 2019
* * * 
### EC2 Virtualization
* Data
* Applications
* Operating System
Line of Shared Responsibility
* Hypervisor
    * Modified of Zen Hypervisor
    * Nitro Hypervisor - Amazon Built from scratch
* Hosts - Hardware
* Physical Space - Walls, Ceiling, AC, Phys. Sec.   

### AMIs
* AWS Marketplace - AWS vetted AMIs
    * Can pay per hour for EC2 AND software license!
* AWS Community - not vetted by anyone, just uploaded/made public in the community

### Starting EC2 Instance
* pick ami
* pick server size
* pick vpc, az, subnet
* User data
    * Runs on first launch for AMI
    * Basically any script
* Add Storage
* Add Tags - Can sort based on tags later
    * Name
    * Asset Tag
    * Owner
    * Dept 
    * Stage - Prod/Dev/Test 
* Configure Security Groups
    * ACLs
* Disk IO happens over network -- possibly in contention with network IO
    * Except for EBS Optimized - basically makes a separate pipe for Disk IO
* Can "expose disks above the line" to EC2 instances - faster - "Ephemeral" Instance Store
    * Ephemeral disk gets wiped when the instance stops
    * Good for swap space, high iops applications
* Instance Types
    * Can pick Variations, Categories, Families, and Types
    * t series - "burstable instances" - RAM is fixed, CPU is shared
* Block storage for EC2 instances
    * SSDs prioritize IOPS
    * HDDs prioritize Throughput
    * Logs, Streaming, Large Files -> sometimes HDDs are better than SSDs?!
* Instance Profile = IAM role for EC2 instance
    * How does the IAM role get enforced for EC2 instance? 
* User Data
    * shell script
    * cloud-config - basically ansible

```
curl http://169.254.169.254/latest/meta-data/
curl http://169.254.169.254/latest/meta-data/instance-id
```
^this command does not go out to the internet, it is intercepted by the hypervisor, specifically looks for ip

* Key Pairs
    * Remote Access - rdp/ssh

* Post Launch Configuration Options
    * Get instance screenshot

```
aws ec2 run-instances
--image-id
--instance-type
--key-name
--security-group-ids
--subnet-id
--iam-instance-profile
--user-data file://<path>
--tag-specifications 
```

* Launch Templates
    * Pre records bascially the above command


Tue Mar 12 18:03:47 EDT 2019
* * * 
### Managing Your EC2 Instances
* Lifecycle of an EC2 Instance
    * When you stop your instance, you stop paying for compute but you still pay for the EBS volume
    * When you terminate, EBS volume is deleted, EC2 instance cannot be restarted
    * Hibernate - memory gets written to disk, reloaded on start, not available for all instance types
* Status Checks - for the instance - basically "did amazon apply power to your instance"
    * Two are shown, one for "above the line" (Instance Status Check), one for "below the line" (System Status Check)
    * Troubleshooting - if above the line, troubleshoot, if below the line, stop and start, EC2 instance will likely drop into a different piece of hardware (based on probably and AWS scheduler)
    * Can Create CloudWatch Alarms to automate reponses - Recover Instance = Start/Stop
* Relaunch Instances
    * Impairment
    * Upgrade of OS architecture or image type
    * Downgrading
    * Auto Scaling
    * Cost savings
    * Best practice - design instances for easy build-up and tear-down
    * "Treat instances as livestock not pets"

* Transitioning to a new instance size
Instance must be stopped

```
aws ec2 modify-instance-attribute
```

* AMI Deprecation
    * Custom AMIs aren't availabe cross region
    * Windows AMIs - deprecated within 5 days of Patch Tuesday
    * Amazon Linux AMIs - available for years
```
aws ssm get-parameters-by-path --path "/aws/service/ami-amazon-linux-latest" --query "Parameters[?Name=='/aws/service/ami-amazon-linux-latest/amzn-ami-hvm-x86_64.gp2'].Value" --output text
```

* * * 
### Securing your EC2 Instances
* Shared Responsibility Model
* Vulnerability Scanning and Pen Testing
    * Policy recently changed
    * Should still notify Amazon for "certain things"

* * * 
### Amazon EC2 Purchasing Options
* On-Demand Instances - pay by hour, per stop/start, each start is new hour
    * Amazon Linux and Ubuntu - can pay by the second, stop/starts get refunded
* Reserved Instances - Significant discount - 1-3 yr terms
* Scheduled Instances - Purchase 1 yr RI for a recurring period of time
* Spot Instances / Blocks - Up to 90% off on demand pricing
    * can set "threshold prices", to stop your instances
    * default interrupt - terminates your instance
    * your instance can be interrupted at any time by AWS - you get a two minute warning
    * can help maximize cpu's per dollar, but not made for important 24/7 tasks
    * Some companies use Spot Instances for Stateless servers, ie web
* Dedicated Hosts - Instances run on h/w dedicated to a single account
* Dedicated Instances - Instances run on h/w dedicated to a single account
* i3.metal - Physical host on which you may run the operating system or virtualization layer

Tue Mar 12 19:04:03 EDT 2019
* * * 
### Elastic Load Balancing
* Managed load balancer, you don't have to worry about the load balancer itself
* Three flavors
    * Classic Load Balancer - prev gen for http, https, and tcp
    * Network Load Balancer - TCP, operates at layer 4
    * Application Load Balancer - HTTP and HTTPS, operates at layer 7, ssl endpoint, can create rules with listeners
```
aws elbv2 create-load-balancer
aws elbv2 create-target-group
aws elbv2 register-targets
aws elbv2 create-listener
aws elbv2 describe-target-health
```

Wed Mar 13 12:21:52 EDT 2019
* * * 
### EC2 Auto Scaling
* Launch or Terminate EC2 Instances (Can't just stop, EBS gets deleted)
* Create a Launch Configuration/Launch Template for Auto Scaler
* Create EC2 Auto Scaling Group
    * Logical Group of EC2 Instances
    * Must set min, max, and desired numbers
* Create Auto Scaling Policy 
    * When and How? 
* Auto Scaling and Instance Health...
* Auto Scaling Termination Policy
    * OldestInstance
    * NewestInstance
    * OldestLaunchConfiguration
    * ClosestToNextInstanceHour
* Can use auto scaling to vertically scale gracefully by changing launch configs when scaling happens (OldestLaunchConfiguration)
* Dynamic Scaling: Amazon EC2
    * Target Tracking Scaling
    * Step Scaling - can create levels of urgency with multiple rules
    * Simple Scaling 
* Instance Warm Up Period - ignore alarms until new instance has booted, prevents overscaling
* Predictive Scaling - ML style
* AWS Suggests targeting 50% CPU usage when using 2 AZs to support an AZ failure

Wed Mar 13 12:56:56 EDT 2019
* * * 
### AWS Route53
* Default ELB DNS Name
    * web-app.us-west-2.elb.amazonaws.com
    * Typically when a dns name points to another name, its a CNAME
    * Name to number is an A record
    * Generally you can't point entire domain to another dns
    * AWS has a feature called ALIAS's that lets you point entire domain to an Amazon fqdn
* Routing Policies
    * Simple Routing
    * Weighted Routing
    * Latency Routing Policy
    * Failover Rouing Policy
    * Geolocation routing policy
    * Geoproximity routing policy 
    * Multivalue answer routing policy 
* Load testing
    * Can do load testing against your own environment

Wed Mar 13 13:24:44 EDT 2019
* * * 
### Containerization
* Problems: Orchestration && Traffic Direction
* Kubernetes - open source container mgt and orchestration
* Amazon EKS - Amazon Elastic Container Service for Kubernetes
1) Provision an EKS cluster
2) Deploy worker nodes
3) Connect to EKS
4) Run container apps

EKS is not managed kubernetes --- it is just the master node

* Amazon Elastic Container Registry (Amazon ECR)
    * Fully managed private Docker repository

Wed Mar 13 14:24:04 EDT 2019
* * * 
### AWS Lambda
* much Serverless, so cloud
* Run code without provisioning or managing servers
* cheaper than running chron jobs on an EC2 instance
* Lamda charges you for run time
* You choose how much memory, Amazon uses that to figure out how much CPU to give you
* When you create your AWS Lambda function, a dedicated container gets created for your code
* Exports logs to cloudwatch - including the output of the function (stdout/stderror)
* Resource limits per invocation
    * max duration: 15 minutes
    * billing: memory x time
* free tier: 1 million executions per month
    * 400,000 gb/seconds per month
* can attach lambda function to a vpc to access resources inside a vpc

Wed Mar 13 14:34:15 EDT 2019
* * * 
### API Gateway
* AWS Services are in the backend processing requests, Lamda, EC2, Dynamo DB, VPC
* Frontend handled by AWS

Wed Mar 13 14:40:45 EDT 2019
* * * 
### API Batch
* Features
    * Granular Job definitions
    * Dynamic Compute resource privisioning and scaling
    * Dynamic Spot Bidding
    * Integrated Monitoring and Logging 
    * Priority based job scheduling

Wed Mar 13 14:42:56 EDT 2019
* * * 
### Amazon Relational Database Service (RDS)
* SQL vs NoSQL
    * SQL = table based, single node, scales well vertically
    * NoSQL = strings/key val pairs, eg json objects, easier to scale horizontally 
* Needs to choose CPU, Memory, network performance, HDD/SDD, provisioned IOPS
* Backup - Manual - creates a storage volume snapshot of your db instance
* Automatic - creates automated backups of DB instance during the backup window
* Fully managed service
* Multi AZ synchronous Replication
    * all is block level
    * except mssql- mirroring
    * when multi az is turned on - backup is created from the secondary
* Read Replicas - higher performance reads
    * "generally dbs are 90:1 read:write ratio"
    * Asynchronous
    * separate database endpoint

Wed Mar 13 15:59:58 EDT 2019
* * * 
### Amazon Aurora
* Mysql and postgresql compatible rel database
* fully mgd service
* virtual data layer
    * split across three AZs
    * synchronous

Wed Mar 13 16:01:19 EDT 2019
* * * 
### DynamoDB
* Non relational
* asynchronous replication
* Access control - IAM - can be as granular as the row
* low latency


Wed Mar 13 16:02:24 EDT 2019

* * * 
### Other Services
* ElastiCache - speed up database services - managed in memory data store services
* RedShift - data warehousing - for petabyte scale databases
* Neptune - Graphing type databases

Wed Mar 13 16:03:55 EDT 2019
* * * 
### AWS Database Migration Service (DMS)
* Migrate your database to AWS with minimal downtime
* Creates a replication instance (EC2)
    * Connects to SQL/Original DB (Source)
    * Target: Some other DB (maybe aurora)
    * Uses "Tasks" to convert data across the databases
* AWS Schema Conversion Tool
    * Can convert from multiple sources to multiple targets

Wed Mar 13 16:17:29 EDT 2019
* * * 
### Amazon VPC
* that moment when you spend 15 minutes on CIDR blocks because of that one sales guy in the back of the room
* Ip Addressing
    * Valid private ip address ranges defined by RFC 1918
    * Can only define networks between /16 and /28 in Amazon VPC
* AZ's networked together with SDN - looks like one hop between AZs
* "Day in the life of a billion packets" - great talk - look in email later
*  VPC is bounded by the Region 
* when you make your subnets, by default they inherit the default route table - one entry - 10.0.0.0/16 - local
* if you want to talk to the internet - add internet gateway - get ig id - add route 0.0.0.0/0 to your ig id
* "traffic doesn't actually flow throught the internet gateway -> SDN"
* What actually controls your bandwidth?
    * Network Perfomance is determined by the Instance type and size of your EC2 instance
    * Look in the Network Performance Column
* Default VPC - in AWS
    * Before 2013 there was no such thing as a VPC
    * You just launched EC2 instance with Public Ip boom done
    * IPs weren't contiguous, came from Amazon's pool, networking nightmare 
    * hence vpc's were created, and now they are mandatory
    * When you launch something without doing any networking, you land in the Default VPC
    * 172.31.0.0./16
* VPC Peering Across Accounts
    * PCX Target - gets created when a peering request is accepted between accounts
    * PCX target appears in the route table for your subnets
    * Will need to point peer subnet at the pcx target
    * Inter account, intra account, cross region
    * Obviously you need to avoid overlapping ip ranges
    * Transitive Peering - Prevented, just because A and B are peered and B and C are peered, C and A are not peered
    * No nat routing between VPCs
    * can be complicated when you have a large number of networks
    * Only cost for VPCs are cross region transactions
* AWS Transit Gateway
    * used when you want to peer large numbers of VPCs
    * Attach VPCs and VPNs together and manage in one place
    * Propagate routes from route tables for the attached VPCs and VPN connections
    * Costs are per AZ attachment per hour/gb
* AWS Simple Monthly Calculator
    * can help with calculating costs
    * doesn't have every service
* Creating VPN Connections in your datacenter
    * Will need to modify route table, add network you want to talk to, point at VPN Gateway id (VGW id)
    * (after creating a VPW)
    * Two IPsec tunnels between AWS VPN aggregator and Customer network
    * Multiple on prem networks can be connected back to VPW
    * VPW can be used as a hub in a hub and spoke methodology to connect multiple on prem networks to each other
* AWS Direct Connect
    * Have to work with a direct connect partner
    * Direct Connect location can be abbreviated as DX location
    * Basically you put your router in the same datacenter as an AWS router, glass to glass connection between the routers


Wed Mar 13 17:26:39 EDT 2019
* * *
### NAT
* To nat private ip addresses to the internet -> route traffic throught the nat gateway
* You must create a default route from your private subnet to the NAT GW id

Wed Mar 13 17:36:08 EDT 2019
* * *
### VPC Gateway Endpoints
* When you spin up a managed service - it doesn't spin up in your subnet
* ^ Hosted in Amazon's network 
* You access the managed service via an HTTPS endpoint 
* VPC Gateway Endpoints allow your VPCs to talk to Managed services without going out to the internet
* Create route to the managed service that sends to the VPC endpoint
* Allows non internet comms to Amazon Managed Service
* Dynamo DB and S3... for all other services use PrivateLink see below
Wed Mar 13 17:40:45 EDT 2019
* * *
### VPC Endpoints with AWS PrivateLink
* gives you an alternative DNS name that you can use, points to endpoint inside your VPC
* AWS partners spin up services in a VPC, now you can access that VPC from your VPC without routing through the internet
* Restricted within Region
* Anyone can create a PrivateLink accessible VPC (used to only be Amazon Partners)

Wed Mar 13 17:47:55 EDT 2019
* * *
### DNS Options within a VPC
* Amazon Route 53 Private Hosted Zones - provides internal DNS naming
* Amazon Route 53 Resolver - inbound, outbound, or both
* .2 - there will automatically be a DNS Resolver here in every subnet
* cant use .3 - reserved for future use
* Subnets in AWS Written in stone, you cannot expand Subnets in a VPC once created

Wed Mar 13 17:51:20 EDT 2019
* * *
### Elastic Network Interfaces
* Virtual NIC attached to EC2 instance
* Up to 15 VNICs per instance
* 50 elastic ip addrs per VNIC
* Use cases - multihomed instances connected to multiple networks
* More VNICs != More Bandwidth

Wed Mar 13 17:53:06 EDT 2019
* * *
### Securing Your network
* Layered defenses
* VPC Route Tables
* Subnet ACLs
* EC2/ENI security Groups
* Thirdparty host based (iptables etc)

Wed Mar 13 17:54:06 EDT 2019
* * * 
### Security Groups
* allow traffic to/from a VNIC
* Stateful - if traffic allowed inbound, the related TCP connection will be allowed outbound
* Config'd be default to 
    * Deny all inbound
    * Allow all outbound
* Can't deny by ip

Wed Mar 13 17:57:00 EDT 2019
* * *
### Network ACLs
* Allow / Deny traffic in and out of subnets
* Default Network ACL
    * Allows all inbound and outbound traffic
* Stateless: Even if rules allow traffic to flow in one direction, you have to explicitly allow in the opposite direction
* Network Acls are firewalls around the subnet
* Eval'd in order of precedence

Wed Mar 13 18:02:20 EDT 2019
* * * 
### Troubleshooting Networks
* Verify its a network issue, not an instance issue
* Check Netowrk ACLS
* Turn on VPC Flow logs, check them
* Network ACL eval'd before Security Group on inbound traffic
* Two most common problem: launched instance in wrong subnet, security group misconfig'd
* By default, ICMP is not allowed


Wed Mar 13 18:16:50 EDT 2019
* * *
### Amazon EBS
* Harddrives for EC2 instances
* network attached
* Persistent block storage
* Boot and Data Volumes supported
* you still pay for EBS volumes when your EC2 instance is stopped
* Stay within their AZ
* Basicallly RAID 1 inside AZ, you don't see or interact with the raid, its just there
* Volume Types: 
    * SSD io1 = provisioned IOPS
    * SSD gp2 = general purpose
    * HDD st1 = Throughput optimized
    * HDD sc1 = Cold HDD
* Creating EBS volumes
    * Tied to AZ
```
aws ec2 create-volume --size 80 --availability-zone us-east-1a --volume-type gp2
```
* EBS optimized instance types - prevent contention with your network traffic when doing IO with the disk
* Amazon EBS incremental Snap shots
    * All the used blocks in your EBS volume get put in the snapshot
    * Saved in S3 -> not in your Console
* Restoring snapshots
    * volumes restored from snapshots have a "first-access" penalty, drive may be slow until everything has been read in
    * can restore Volumne in any AZ or Region 
```
aws ec2 create-volume --size 80 --availability-zone us-east-1a --volume-type gp2 --snapshot-id <snapshot id>
```
* Snapshot creation
    * snapshot command returns asynchronously
    * stop or quiesce the disk/database - for consistency, critical for database serverss, RAID configs, no subsequent writes are captured, use fs_freeze on linux
    * remains in pending state until finished
* Lifecycle Management for snapshots
    * Amazon Data Lifecycle Manager (DLM)
    * manages snapshots for you
    * use AWS console
    * use command line


Wed Mar 13 19:17:23 EDT 2019
* * * 
### Instance Store
* Definition: Block Level storage on a shared disk subsystem
* way faster than EBS
* ephemeral - wiped when EC2 is stopped
* good for swap space

Wed Mar 13 19:19:17 EDT 2019
* * *
### Amazon Elastic File System
* managed NFS
* NFS 4.0 and 4.1 supported 

Wed Mar 13 19:20:44 EDT 2019
* * *
### Amazon FSx for Windows File Server
* can be integrated with AD
* essentially a windows file share
* FSx Luster = not for windows, some kind of high performance computing


Wed Mar 13 19:22:34 EDT 2019
* * *
### Amazon S3
* simple storage service
* great for static data
* "object storage" - distinct from block storage
* when you update an object, you update the whole object
* 99.999999999% durability (11 nines)
* 99.99% availability
* no limit on the amount of data stored
* bucket name is globally unique
* make sure it is not public
* Bucket Policy 
    * written in json
    * IAM doesn't apply to public buckets
    * explicity deny's take precendence
    * Policy Generator tool - UI for creating bucket policy
* Can turn on versioning to be able to access older versions of files
* Versioning is not de duplicated, complete copies, you pay for the space consumed
* If data is leaving region, you pay b/w cost
* You pay for storage
* You pay per API call against S3
* S3 Analytics - Tool that shows you access records for your S3 bucket
* Intelligent Tiering - Automatically Stores your data in the cheapest tier

Wed Mar 13 20:01:27 EDT 2019

* * * 
### Amazon S3 Glacier
* Glacier doesn't have URLs
* You have to initial a "retrieve job" to get your files
* Standard Retrieval Time is 3-5 hours
* Expedited 1-5 minutes
* Bulk is 5-12 hours
* Just slow for pricing model, not really that slow
* Most customers move data into glacier via lifecycle policies

Thu Mar 14 12:11:39 EDT 2019
* * *
### AWS Transfer for SFTP
* spin up a transfer endpoint, get url, send data to url

Thu Mar 14 12:13:09 EDT 2019
* * *
### AWS Storage Gateway
* run on prem
* vm that you download and run locally
* can make it into a file gateway, replicates every file stored in its shares in the cloud
* Tape Gateway - pretends to be tape, point your backup server at it, actually writes to the cloud

Thu Mar 14 12:16:34 EDT 2019
* * *
### AWS Data Sync
* More generic, managed service
* basicallly just tell AWS source and target and it happens

Thu Mar 14 12:17:46 EDT 2019
* * * 
### AWS Snowball
* Shipped to you, put your data on it, ship it back to amazon
* Snowmobile - full sized shipping container pulled by a semi truck - 20 Petabytes
* Snowball edge - capacity up to 100 GB - has flavors of compute 
* AWS Outposts - future - local on prem rack 

Thu Mar 14 12:28:40 EDT 2019
* * *
### Amazon Cloudwatch
* Monitoring and mgt service that provides data and actionable insights for your AWS Resources
* Aggregator of information

Thu Mar 14 13:20:36 EDT 2019
* * *
### Amazon Cloudwatch Monitoring
* monitor the state and utilization of most of the resources
* Key concepts
    * standard metrics - things aws can see (below the line)
    * custom metrics -
    * alarms
    * notifications
* CloudWatch Agent - collect system level metrics
    * Amazon Ec2 instacnes
    * on prem servers - may not want to do that
* Metric Components
    * Metrics (name + val)
    * Namespace
    ....
* Monitoring and Security
    * nothing special... 
* Using CloudWatch and Alarms
* CloudWatch Automatic Dashboards 
    * Can export dashboard to another dashboard...
* By Default EC2 instances report every 5 mins (free tier)

Thu Mar 14 13:32:35 EDT 2019
* * *
### Amazon CloudWatch Events
* Allows you to react in real time

Thu Mar 14 13:33:36 EDT 2019

* * * 
### Amazon CloudWatch Logs
* Log Aggregator
* Alerting based on things in the logs
* Is there multi event correlation? 
* Can filter - Count events - alert based on a number 


Thu Mar 14 13:36:06 EDT 2019
* * *
### AWS CloudTrail
* Audit log for AWS Api and Accounts
* Free tier is a runnning 90 day window - minus data plane actions (s3)
* paid tier doesn't expire, contains data plane
* Regional


Thu Mar 14 13:40:52 EDT 2019
* * * 
### AWS Config
* Takes an inventory of all services in account
* Gives configuration history of all services
* Visibility into human errors
* AWS Config Rules
    * setup rules to configure environment
    * doens't stop things from happening, marks non compliance 

Thu Mar 14 13:44:11 EDT 2019
* * *
### Security 
* Artifact - compliance reports for below the line compliances
* Cloud Directory - Not LDAP, managed service
* Directory Service - Managed Active Directory
* Inspector - agent, checks for known CVEs on your software
* Macie - Machine Learning - point at S3 bucket - will figure out type of data on objects
    * Can help you find data that you need to protect 
    * eg PII, PCI, etc
* Certificate Manager - free certs for certain aws services
* CloudHSM - (Hardware Security Module) - FIPS level 3 encryption
* KMS - Key Management System - manages your encryption keys
    * May not be compliant for your need to encryption keys
    * if you need to be the single tenant owner of your encryption engine - HSM
* Shield - DDOS protection - AWS builds in automatically for free, managed endpoints only
    * EC2 instances are not protected
    * If you are behind a managed service like ELB, Shield Standard applies
    * Shield Advanced - includes WAF - plus support team 24/7, cost protection for additional EC2 instances/costs associated with attacks
* AWS WAF - charged service 
    * allows you to add more rules for DDOS prevention
    * Runs at the edge locations

Thu Mar 14 13:58:59 EDT 2019
* * *
### AWS Guardduty
* Basic IDS
* Brute force detection, scanning
* New location logins
* Attempts to define baseline for normal activity, then alerts on abnormal activity
* Looks for Bitcoin mining patterns
* pen test signatures
* data exfil signatures
* Can generate a lot of false positives
* Can set whiteliest - eg known good traffic
* Analyzes cloudtrail, vpc flow logs, and cloudwatch all together

Thu Mar 14 14:04:25 EDT 2019
* * *  
### Using Tagging in AWS
* Can take actions based on what things are tagged
* Examples: name, owner, application, cost center, purpose, stack
* Can Edit Multiple Tags at once with Tag Editor 
* Resource Groups >> Tag Based
* Case Sensitive 
* Make sure you have a standard
* Can enforce tags with AWS Config
```
aws ec2 create-tags --resources <id> --tags "Key=SecurityCheck,Value=$TIMESTAMP"
aws ec2 describe-instances --filters "Name=tag-key, Values=SecurityCheck" 
```

Thu Mar 14 15:53:53 EDT 2019
* * * 
### Cost Benefits of the Cloud
* Pay for only what you need
* Create scripts/templates to shutdown environments
* Unused resources can be turned off
* Creating a "Stopinator"
    * Script to turn on and turn off selected AWS Resources
    * Best practices to reduce cost
* Can sort bill by tags, enables sorting billing by department etc
* Netflix - Simian Army tools
    * Chaos monkey - randomly kills production instances
    * Conformity monkey - enforces tags by killing non conforming instances
* Enforcing Tagging with IAM
    * Conditions - can be applied to IAM policies, doesn't always give you a useful error  
* Ability to see billing dashboard is a separate permission, even separate from Administration, must be granted as root - see email from Alan for instructions
* Can make budgets as well
* Use Cloudwatch to get metrics to know if you are using the right h/w for your apps
* Use the Trusted Advisor Dashboard!!!!

Thu Mar 14 16:50:40 EDT 2019
* * * 
### Configuration Management in the Cloud
* Userdata, AMIs, Config and Deployment Frameworks, AWS OpsWorks, AWS CloudFormation
* OpsWorks - managed puppet or chef
* AMI Creation Details
    * linux - can create ami from disk snapshot
    * windows - must run sysprep to strip instance specific networking information
    * Amis are tied to regions, if you want to use in another region, you must copy it 
    * Copy may fail if AWS cannot find a corresponding Amazon Kernel Image (AKI) in the region - for custom linux kernels
* Using a configuration Server
    * Can greatly simplify admin tasks
    * Configuration is idempotent, resources only allocated once, manual changes detected and rolled back
    * Supply user data to instance to kick off configuration


Thu Mar 14 17:31:42 EDT 2019
* * *
### AWS CloudFormation
* Build your cloud into a template, defines your environment
* Desired State language 
* Template is JSON or YAML
* Stack is created from Template
* If you want to update the stack -> update the template -> send update command 
* Changeset - shows you the difference between your current environment and your desired environment
* Structure of a Template - use templates and tools for drag/drop 
* Parameters - inputs
* When cloud formation template has a problem - AWS rolls back to last known good, can be overridden for troubleshooting
* Can pass roles to CloudFormation during Stack Creation
* can estimate your cost before you run
* Mappings - "when im in this region, get this ami" - for region specific resources
* "Ref" - variable, refers back to thing being built somewhere else by cloud formation
    * Essentially a dependency
* aws-cfn-bootstrap 
    * scripts that help with CloudFormation 
    * cfn-init - can put this in your user data instead of scripts CloudFormation::Init
    * cfn-signal - tells cloudformation when user data is done, put at the end of your user data script
* outputs
    * can tell the user information about instances etc
    * when you have more than one template in play
    * Separate your templates by type, eg networks, servers, etc
    * Call each module with a centralized template
    * Outputs of one stack can be the parameters of another stack
* recommends not using vi, use and ide instead...     :.....(
* troposphere - create template with python objects
* errors
    * cloud-init.log, cfn-init.log, cfn-wire.log
    * tip: use cloudwatch logs to auto update the logs
* Do not embed credentials in your templates
* Drift Detection - not real time - can detect changes to the stack that are different from the CloudFormation

Thu Mar 14 18:20:10 EDT 2019

* * * 
### CloudFormation Troublshooting
* there is a guide: CloudFormation Troubleshooting Guide

* * *
### how to pass the SYSOPS Associate Exam
* aws.training.com
* Go to certification page at the top 
* Exam Prep Resources
* Do the practice q's 
* Read the study guide - not all are current - sysops is current - avoid kindle version for answer flipping
* Study AWS Whitepapers and FAQ - specifically Security Best Practices and FAQs
* Don't worry about Exam Readiness training
* Take practice exam $20 for 20 question exam - good gut check
* If they offer you a sheet of paper at the test center - take it!
* Class gets you 60% of the way
* Cert is good for 3 years 
