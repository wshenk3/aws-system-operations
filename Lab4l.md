### Configuring VPC

##### Task 1: Create a VPC

Console >>> VPC >> Your VPCs >> Create VPC 
Name: Lab VPC
IP block: 10.0.0.0/16

Create >> Close

Select Lab VPC

Actions >> Edit DNS Hostnames

select enable >> Save >> close

##### Task 2: Create Subnets

Subnets >> create subnet 
Name: Public Subnet
VPC: Lab VPC
AZ: Select the first one in the list
IP: 10.0.0.0/24
Create >> Close

Select Public Subnet >> Modify Auto Assign IP Settings
Select Auto Assign IPv4
Save

Subnets >> create subnet
Name: Private Subnet
VPC: Lab VPC
AZ: Select the first one in the list
IP: 10.0.2.0/23

##### Task 3: Create Internet Gateway

Internet Gateways >> Create Internet Gateway
Name: Lab IGW
Create >> Close

Select Lab IGW
Actions >> Attach to VPC >> Lab VPC >> attach


##### Task 4: Configure Route Tables
Route Table 
Select Lab VPC Route Table
Click the name Columen: enter "Private Route Table"

Click the Routes tab in the lower half of the page
Click Create Route Table
Name: Public Route Table
VPC: Lab VPC
Create >> Close

Select Public Route Table
Edit Routes >> Add Route
Destination: 0.0.0.0/0
Target: Select Internet Gateway then select Lab igw
Save Routes >> close


Click Subnet Associations Tab >> Edit subnet associations
Select the row with "Public Subnet"
Click Save

##### Task 5: Launch a Bastion Server in the Public Subnet
Services >>  EC2 >> Launch Instance 

AMI: Linux 2
Instace: t2.micro
Network: lab vpc
subnet: public subnet
Storage: default
Tags: Name: Bastion Server
Security Group: Default
Launch >> View instances


##### Task 6: Create a NAT Gateway

Services >> VPC >> NAT Gateways
Create NAT Gateway 

Subnet: Public Subnet
Click Create new EIP
Create NAT Gateway
CLick Edit Route table

Select Private Route Table

Click the routes tab in the lower half of the page

Click Edit Routes

Click Add Route

Destination: 0.0.0.0/0
Target: Select NAT Gateway the nat- from the list
Save Routes >> CLose


##### Optional Task: Test the Private Subnet
Services >> EC2 >> Launch Instance 
AMI: Linux 2
Instance: t2.micro
Network: lab vpc
Subnet: Private Subnet
User data:
```
#!/bin/bash
# Turn on password authentication for lab challenge
echo 'lab-password' | passwd ec2-user --stdin
sed -i 's|[#]*PasswordAuthentication no|PasswordAuthentication yes|g' /etc/ssh/sshd_config
systemctl restart sshd.service
```
Storage: Default
Tags: Name: Private Instance
Security Group: Default

Launch >> View Instances

login to bastion host
```
chmod 400 ~/Downloads/qwikLABS-L3865-1932963.pem
ssh -i ~/Downloads/qwikLABS-L3865-1932963.pem ec2-user@ec2-34-222-4-182.us-west-2.compute.amazonaws.com
```
log on to private instance
```
ssh 10.0.3.148
lab-password
```

test nat gateway
```
ping -c 3 amazon.com
```

