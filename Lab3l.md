### Lab3L: Using Auto Scaling in AWS (Linux)
* * *
##### Task 1: Create a New Amazon Machine Image for Amazon EC2 Auto Scaling

EC2 >> Instances >> Command Host >> Copy Public IP

Download PEM from Qwiklabs
```
chmod 400 ~/Downloads/qwikLABS-L3863-1932319.pem

ssh -i ~/Downloads/qwikLABS-L3863-1932319.pem ec2-user@54.188.164.189
```

View the UserData.txt file

```
vi UserData.txt
```

Copy KeyName, AmiId, SubnetId, and HTTPAccess from Qwiklabs into the below command 
```
aws ec2 run-instances --key-name <key-name> --instance-type t2.micro --image-id <ami-id> --user-data file:///home/ec2-user/UserData.txt --security-group-ids <sg-id> --subnet-id <subnet-id> --associate-public-ip-address --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=WebServerBaseImage}]' --output text --query 'Instances[*].InstanceId'

aws ec2 run-instances --key-name qwikLABS-L3863-1932319 --instance-type t2.micro --image-id ami-a0cfeed8 --user-data file:///home/ec2-user/UserData.txt --security-group-ids sg-057c06fce141da841 --subnet-id subnet-09527a783f1a3eda8 --associate-public-ip-address --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=WebServerBaseImage}]' --output text --query 'Instances[*].InstanceId'
```
Results:
```
i-0c6cd41a65296c02a
```

Query your new instances
```
aws ec2 describe-instances --instance-id i-0c6cd41a65296c02a --query 'Reservations[0].Instances[0].NetworkInterfaces[0].Association.PublicDnsName'
```
Results:
```
"ec2-34-220-158-148.us-west-2.compute.amazonaws.com"
```

Navigate to website
```
http://ec2-34-220-158-148.us-west-2.compute.amazonaws.com/index.php
```

Create a Custome AMI based on the image that was just created
```
aws ec2 create-image --name WebServer --instance-id i-0c6cd41a65296c02a
```
Results:
```
{
    "ImageId": "ami-0bfe7daab94c0b7b4"
}
```


* * *
##### Task 2: Create an Auto Scaling Environment

Go to EC2 Console >> Load balancers >> Create Load Balancer

Under Application Load Balancer >> Create

Name: webserverloadbalancer

AZ >> VPC: Lab Vpc

Click the first displayed AZ >> Click Public Subnet 1 

Click the second displayed AZ >> Click Public Subnet 2

Next: Configure Security Settings

Next: Configure Security Groups

Select HTTPAccess

Next: Configure Routing >> Name: webserver-app

Expand Advanced Health Check Settings >> Configure

Path: /index.php
Healthy Threshold: 2
Interval: 10

^ sets up a Health Check to be performed every 10 seconds, instance must respond twice in a row to be healthy

Next: Register Targets

Next: Review

Create >> Close



Create Launch Configuration

EC2 Console >> Auto Scaling Groups >> Create Auto Scaling Groups >> Get Started

Choose AMI >> My AMIs >> WebServer: Select

Choose Instance Type: t2.micro

Next: Configure Details

Configure Details

Name: WebServerLaunchConfiguration

Monitoring: enable Cloudwatch detailed monitoring

Next: Add Storage

Next: Configure Security Group

Configure Security Group Select HTTPAccess

Review 

Continue

Create Launch Congfiguration

Ack and Create



Create and Auto scaling Group

Group name: WebServersASGroup
Group Size: start with 2 instances
Network: Lab VPC
Subnet: private subnet 1 and private subnet 2

Advanced Details
Load Balancing: Select "Recv Traffic from one or more load balancers"
Target Groups: webserver-app
Health Check Type: Select ELB
Monitoring: Select Enable CloudWatch Detailed Monitoring

Next: configure Scaling policies

Select Use Scaling Policies to adjust capacity of this group

Specify scaling between 2 and 4 instances

Scale the auto scaling group using step or simple scaling policies 


Specify a Scale Out Policy

Under Increase Group Size, accept default name

Click add new alarm

Clear the Send a notification checkbox

Whenever: Average of: CPU Utilization

is: >= 50 percent

For at least: 1 consecutive periods of 1 minute

Name of Alarm: WebServerScaleOutAlarm

Click create alarm

Under Increase Group Size

Create a simple scaling policy link

Take the action: add 1 instances



Specify a Scale In Policy

Under Decrease Group Size

Leave name defalut, click add new alarm link

Create Alarm
* Clear the send a notification checkbox
* Whenever: Average
* Of: CPU Utilization
* is: <30 percent
* For at least: 1 consecutive period of: 1 minute
* Name of alarm: WebServerScaleInAlarm

Create alarm

Click Create a simple scaling policy linik

take the action: Remove 1 instances

Next: Configure Notifications

Next: Configure Tags

Enter: Name (under Key) and WebApp (under Value)


Click Review

Click Create Auto Scaling Group

Close


Verify the Auto Scaling Configuration


EC2 >> Instances >> Verify Two instances of WebApp are running

Observer Status Checks field

Click Target Groups >> Select webserver-app

On the targets tab, refresh until status of instances changes to healthy


Click Load Balancers >> select webserverloadbalancer

Under Description >> Copy DNS name


browse to 

```
http://webserverloadbalancer-1881981180.us-west-2.elb.amazonaws.com/index.php
```

On webpage >> click Start Stress

In navigation pane: Click Auto Scaling Groups

Select WebServerASGroup

Select Activity History, after a few mins you should see Auto Scaling Group add an instance





* * *
* * *
* * *


