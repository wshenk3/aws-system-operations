# Lab1: Using AWS Systems Manager

* * *
### Task 1: Generate Inventory Lists for Managed Instances
Systems Manager >> Managed Instances >> Select Instance >> Setup Inventory

Name: Inventory-Association
Targets: Manually Selecting Instances
Select Managed Instance

Click Setup Inventory at the bottom of the page

AWS Systems mgr will now regularly inventory the instance for selected properties

Click the instance id displayed in the Managed Instances Row

Click the Inventory Tab

List of applications installed will be displayed

* * * 
### Task 2: Install a custom application using run command

Click Run Command

Click Run Command again

Click the Magnifying glass in the search bar >> Owner >> Owned by me >> Name of Document

Click Content tab

This shows the appliation to be installed

Close tab

Select the Document 

Select the Target... the managed instance

Click Run

Can also be accomplished with the below command 
```
aws ssm send-command --document-name "qls-1928556-260beb5864e67ea6-InstallDashboardApp-10NRBRUTZJJQV" --document-version "1" --targets "Key=instanceids,Values=i-099c270f61c23b674" --parameters '{}' --timeout-seconds 600 --max-concurrency "50" --max-errors "0" --region us-west-2
```
* * * 
### Task 3: Use Paramter Store to Manage Application Settings

Click Shared Resources >> Paramater Store

Click create paramter and configure per lab

Refresh web page > now that you config'd the paramter to be true, the website can query that paramter and change output

* * * 

### Task 4: Use Session Manager to Access instances

Click Session Manager >> Start Session

Select Instance >> Start Session

```
ls /var/www/html

AZ=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
export AWS_DEFAULT_REGION=${AZ::-1}

aws ec2 describe-instances
```
^The agent runs the command

To setup Systems manager
1) There must be an agent on the instance
2) The role must be granted to the agent to talk to Systems Manager
3) You have to have permission as a user to access Systems manager


 

