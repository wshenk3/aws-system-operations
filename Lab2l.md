# Lab 2L: Creating Amazon EC2 Instances (Linux)


### Task 1: Launch Instance from AWS Console
EC2 >> Launch Instance >> Amazon Linux 2 >> t2.micro >> Network/Subnet/IAM Role >> Add Storage >> Add Tags >> Security Group >> Review Launch >> Select Key Pair >> Ack >> Launch 

### Task 2: Login to Bastion Server
View Instances >> Select Instance >> Look at Metadata for Ip

Downlaod PEM from Qwiklabs

```
chmod 400 ~/Downloads/qwikLABS-L3861-1929057.pem
ssh -i ~/Downloads/qwikLABS-L3861-1929057.pem ec2-user@52.42.20.199
```

### Task 3: Launch an Instance with AWS CLI

Obtain the AMI to use
```
# Set the Region
AZ=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
export AWS_DEFAULT_REGION=${AZ::-1}

# Obtain latest Linux AMI
AMI=$(aws ssm get-parameters --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 --query 'Parameters[0].[Value]' --output text)

echo $AMI
```
Results:
```
ami-095cd038eef3e5074
```

Obtain the Subnet to Use
```
SUBNET=$(aws ec2 describe-subnets --filters 'Name=tag:Name,Values=Public Subnet' --query Subnets[].SubnetId --output text)

echo $SUBNET
```
Results: 
```
subnet-0c05957e6df4e185a
```

Obtain the Security Group to use
```
SG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=WebSecurityGroup --query SecurityGroups[].GroupId --output text)

echo $SG
```
Results: 
```
sg-0e71f8acfcc3bb707
```

Download a User Data Script
```
wget https://us-west-2-tcprod.s3.amazonaws.com/courses/ILT-TF-100-SYSOPS/v3.3.2/lab-2-ec2-linux/scripts/UserData.txt
cat UserData.txt
```
Results:
```
#!/bin/bash
# Install Apache Web Server
yum install -y httpd

# Turn on web server
systemctl enable httpd.service
systemctl start  httpd.service

# Download App files
wget https://us-west-2-tcprod.s3.amazonaws.com/courses/ILT-TF-100-SYSOPS/v3.3.2/lab-2-ec2-linux/scripts/dashboard-app.zip
unzip dashboard-app.zip -d /var/www/html/
```
Launch a new Instance
```
INSTANCE=$(\
aws ec2 run-instances \
--image-id $AMI \
--subnet-id $SUBNET \
--security-group-ids $SG \
--user-data file:///home/ec2-user/UserData.txt \
--instance-type t2.micro \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Web Server}]' \
--query 'Instances[*].InstanceId' \
--output text \
)

echo $INSTANCE
```
Results:
```
i-011f0bfa9763839a2
```
Query instance for status
```
aws ec2 describe-instances --instance-ids $INSTANCE
aws ec2 describe-instances --instance-ids $INSTANCE --query 'Reservations[].Instances[].State.Name' --output text
```

Test Web Server
```
aws ec2 describe-instances --instance-ids $INSTANCE --query Reservations[].Instances[].PublicDnsName --output text
```
Results:
```
ec2-35-160-186-222.us-west-2.compute.amazonaws.com
```
^browse to this site to test your instance's web server

### Challenge 1: Connect to Amazon EC2 Instance
##### Tasks
* Obtain DNS Name of Misconfigured Server
* Establish SSH Connection to the instance
* Diagnose why this does not work and fix the misconfiguration
```
aws ec2 describe-instances --query 'Reservations[].Instances[*].[InstanceId, State.Name, PublicDnsName, Tags]'
```
DNS Name for Misconfig'd Server:
```
ec2-35-167-179-77.us-west-2.compute.amazonaws.com
```
```
ssh -i ~/Downloads/qwikLABS-L3861-1929057.pem ec2-user@ec2-35-167-179-77.us-west-2.compute.amazonaws.com
```
^ ssh seems closed, check security group
yep, ssh was missing, added port

```
ssh -i ~/Downloads/qwikLABS-L3861-1929057.pem ec2-user@ec2-35-167-179-77.us-west-2.compute.amazonaws.com
```
worked

### Challenge 2: Fix the Web Server Installation
##### Tasks
* Point web browser to misconfig'd server
* Why does website not appear?
* Diagnose and fix instance
```
ec2-35-167-179-77.us-west-2.compute.amazonaws.com
```
web server not running on instance...

looking at user data file
echos errors to /tmp/errors.txt
```
ssh -i ~/Downloads/qwikLABS-L3861-1929057.pem ec2-user@ec2-18-236-167-67.us-west-2.compute.amazonaws.com

cat /tmp/error.txt
httpdd: unrecognized service

misspelling in user data

service httpd start
Starting httpd: (13)Permission denied: make_sock: could not bind to address [::]:80
(13)Permission denied: make_sock: could not bind to address 0.0.0.0:80
no listening sockets available, shutting down
Unable to open logs
                                                           [FAILED]
sudo service httpd start
```


