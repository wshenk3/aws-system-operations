### Lab 6l: Monitoring Your Applications and Infrastructure

##### Task 1: Install the CloudWatch Agent

Services >> Systems Manager >> Run Commmand >> Run a Commmand >> AWS-ConfigureAWSPackage
Command Parameters
Action: Install
Name: AmazonCloudWatchAgent
Version: Latest

Targets >> Select Web Server >> Click Run

Wait for Overall Status to show Success

Targets and Outputs >> Click the instance name displayed under instance id

Expand Step 1- Output 

Left Nav Pane >> Paramter Store >> Create Parameter 

Name: Monitor-Web-Server
Description: Collect web logs and system metrics
Value: see below 
```
{
  "logs": {
    "logs_collected": {
      "files": {
        "collect_list": [
          {
            "log_group_name": "HttpAccessLog",
            "file_path": "/var/log/httpd/access_log",
            "log_stream_name": "{instance_id}",
            "timestamp_format": "%b %d %H:%M:%S"
          },
          {
            "log_group_name": "HttpErrorLog",
            "file_path": "/var/log/httpd/error_log",
            "log_stream_name": "{instance_id}",
            "timestamp_format": "%b %d %H:%M:%S"
          }
        ]
      }
    }
  },
  "metrics": {
    "metrics_collected": {
      "cpu": {
        "measurement": [
          "cpu_usage_idle",
          "cpu_usage_iowait",
          "cpu_usage_user",
          "cpu_usage_system"
        ],
        "metrics_collection_interval": 10,
        "totalcpu": false
      },
      "disk": {
        "measurement": [
          "used_percent",
          "inodes_free"
        ],
        "metrics_collection_interval": 10,
        "resources": [
          "*"
        ]
      },
      "diskio": {
        "measurement": [
          "io_time"
        ],
        "metrics_collection_interval": 10,
        "resources": [
          "*"
        ]
      },
      "mem": {
        "measurement": [
          "mem_used_percent"
        ],
        "metrics_collection_interval": 10
      },
      "swap": {
        "measurement": [
          "swap_used_percent"
        ],
        "metrics_collection_interval": 10
      }
    }
  }
}
```
The above config defines the floowing items to be monitored
Logs: Two web server log files to be collected and sent to Amazon CloudWatch Logs
Metrics: CPU, disk, and memory metrics to send to Amazon CloudWatch Metrics

Click Create Parameter >> Run Command >> Run Command >> Click Magnifying Glass
Document Name Prefix
Equal
AmazonCloudWatch-ManageAgent

Press Enter >> Click AmazonCloudWatch-ManageAgent >> Click Content >> Scroll to bottom to see actual script that will run on a target instance 

Close Tab >> Return to "Run a command" tab

Select AmazonCloudWatch-ManageAgent >> Command Parameters
Action: configure
Mode: ec2
Optional Configuration Source: ssm
Optional Config Locatino: Monitor-Web-Server
Optional Restart: yes

Targets >> select Web Server >> Click Run

Wait for overall status to change to success

##### Task2: Monitor Application Logs using CloudWatch Logs

Services >> CloudWatch >> Logs >> Http Access Log

Logs Streams - click value displayed under here

Left Nav Pane >> Logs >> Select HttpAccessLog >> Create Metric Filter >> paste below into filter pattern

```
[ip, id, user, timestamp, request, status_code=404, size]
```

Click test pattern >> check results >> Assign Metric 

Configure Metric Details
Namespace: LogMetrics
Name: 404Errors
Click Create Filter

Click Create Alarm >> Settings
Name: 404ErrorNotification
Desc: Alert when too many 404s detected on an instance
Whenever: 404Errors
is: >= 5
for 1 out of 1 datapoints

Actions >> verify defaults >> if you do not see them click +Notification
Whenever this alarm: state is alarm
send notification to: select a notification list

Click new list
Send Notification: Administrator
Email List: Enter an email address that you can accesss 

Click Edit at the to pof the page
Change Period: 1 Minute
Click Select Metric >> Create alarm


Confirm >> View Alarm

Left Nav Pane >> CloudWatch >> alarm should appear in orange 

Refresh web page 5 times

check email & look at alarm 


##### Task 3: Monitor Instance Metrics using CloudWatch 
EC2 >> Instances >> Select Web Server >> Click Monitoring 
Examine metrics

Click View All Cloudwatch metrics >> Click CWAgent >> Device, fstype, host, path

Click CWAgent >> Click host >> Click All

##### Task 4: Create Real Time Notifications

CloudWatch Events
Left nav pane >> Click Rules >> Create Rule >> Event Source
Service Name: EC2
Event Type:EC2 Instance State-change Notification
Select specific states >> From drop down select stopped and terminated
Targets: Click Add Target >> Lambda Function >> SNS topic
Topic: Administrator
Click Configure Details
In Rule Definition configure:
Name: Instance_Stopped_Terminated
Click Create Rule


Services >> Simple Notification Service >> topics >> ARN (click link here)

Click Create Subscription, configure
Protocol: SMS
Endpoint: Enter your cellphone in international format
Click Create Subscription

Services >> EC2 >> Instances >> Web Server >> Actions >> Stop 

^ should trigger text message

##### Task 5: Monitor Your Infrastructure for Changes

Services >> Config >> Get Started >> Next >> Skip >> Confirm

Rules >> Click Add Rule >> Search: required-tags >> Click required-tags >> Scroll Down to Rule Parameters

to the right over tag1Key enter: project
Click Save >> Click Add Rule

Search ec2-volume-inuse-check

Click ec2-volume-inuse-check check box
CLick Save

Click Refresh





