### Lab 7: Managing Resources (Linux)

##### Task 1: Using Tags to Manage Resources
```
chmod 400 ~/Downloads/qwikLABS-L3870-1937047.pem
ssh -i ~/Downloads/qwikLABS-L3870-1937047.pem ec2-user@ec2-54-187-49-239.us-west-2.compute.amazonaws.com
```

Finding Development Instances for a Project
```
aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem"

aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" --query 'Reservations[*].Instances[*].InstanceId'

aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" --query 'Reservations[*].Instances[*].{ID:InstanceId,AZ:Placement.AvailabilityZone}'

aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" --query 'Reservations[*].Instances[*].{ID:InstanceId,AZ:Placement.AvailabilityZone,Project:Tags[?Key==`Project`] | [0].Value}'

aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" --query 'Reservations[*].Instances[*].{ID:InstanceId,AZ:Placement.AvailabilityZone,Project:Tags[?Key==`Project`] | [0].Value,Environment:Tags[?Key==`Environment`] | [0].Value,Version:Tags[?Key==`Version`] | [0].Value}'

aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" "Name=tag:Environment,Values=development" --query 'Reservations[*].Instances[*].{ID:InstanceId,AZ:Placement.AvailabilityZone,Project:Tags[?Key==`Project`] | [0].Value,Environment:Tags[?Key==`Environment`] | [0].Value,Version:Tags[?Key==`Version`] | [0].Value}'
```


Changing Version Tag for Development Process
```
vi change-resource-tags.sh
```
Contents:
```
#!/bin/bash

ids=$(aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" "Name=tag:Environment,Values=development" --query 'Reservations[*].Instances[*].InstanceId' --output text)

aws ec2 create-tags --resources $ids --tags 'Key=Version,Value=1.1'
```

Run 
```
./change-resource-tags.sh
```

Verify Version Number on instances incremeneted
```
aws ec2 describe-instances --filter "Name=tag:Project,Values=ERPSystem" --query 'Reservations[*].Instances[*].{ID:InstanceId, AZ:Placement.AvailabilityZone, Project:Tags[?Key==`Project`] |[0].Value,Environment:Tags[?Key==`Environment`] | [0].Value,Version:Tags[?Key==`Version`] | [0].Value}'

```

##### Task 2: Stop and Start Resources By Tag

```
cd aws-tools
vi stopinator.php
```
Contents:
```
#!/usr/bin/php
<?php
# A simple PHP script to start all Amazon EC2 instances and Amazon RDS
# databases within all regions.
#
# USAGE: stopinator.php [-t stop-tags] [-nt exclude-tags]
#
# If no arguments are supplied, stopinator stops every Amazon EC2 and
# Amazon RDS instance running in an account.
#
# -t stop-tag: The tags to inspect to determine if a resource should be
# shut down. Format must follow the same format used by the AWS CLI.
#
# -e exclude-id: The instance ID of an Amazon EC2 instance NOT to terminate. Useful
# when running the stopinator from an Amazon EC2 instance.
#
# -p profile-name: The name of the AWS configuration section to use for
# credentials. Configuration sections are defines in your .aws/credentials file.
# If not supplied, will use the default profile.
#
# -s start: If present, starts instead of stops instances.
# PREREQUISITES
# This app assumes that you have defined an .aws/credentials file.

require 'vendor/autoload.php';
use Aws\Ec2\Ec2Client;

date_default_timezone_set('UTC');

# Obtain the profile name.
$profile = "default";
$opts = getopt('p::t::e::s::');
if (array_key_exists('p', $opts)) { $profile = $opts['p']; }

$excludeID = "";
if (array_key_exists('e', $opts)) {
        $excludeID = $opts['e'];
}

$start = false;
if (array_key_exists('s', $opts)) {
        $start = true;
}

$ec2DescribeArgs = array();
if (array_key_exists('t', $opts))
{
        $tagArray = array();
        foreach (explode(";", $opts['t']) as $pair) {
                $nameVal = explode("=", $pair);
                array_push($tagArray, array(
                        'Name' => "tag:" . $nameVal[0],
                        'Values' => array($nameVal[1])
                        ));
        }

        $ec2DescribeArgs['Filters'] = $tagArray;
}

# Iterate through all available AWS regions.
$ec2 = Ec2Client::factory(array(
        'profile' =>$profile,
        'region' => 'us-east-1'
));

$regions = $ec2->describeRegions();
foreach ($regions['Regions'] as $region) {
        $instanceIds = array();

        # Find resources in each region.
        echo 'Region is ' . $region['RegionName'] . "\n";
        $ec2Current = Ec2Client::factory(array(
                'profile' => $profile,
                'region' => $region['RegionName']
        ));
        $result = $ec2Current->describeInstances($ec2DescribeArgs);
        foreach ($result['Reservations'] as $reservation) {
                foreach ($reservation['Instances'] as $instance) {
                        # Check that this is not an excluded instance.
                        if (strlen($excludeID) > 0 && $excludeID == $instance['InstanceId']) {
                                echo "\tExcluding instance " . $instance['InstanceId'] . "\n";
                        } else {
                                # Is it a running instance?
                                if ($start) {
                                        if ($instance['State']['Code'] == 80) {
                                                array_push($instanceIds, $instance['InstanceId']);
                                                echo "\tIdentified instance " . $instance['InstanceId'] . "\n";
                                        } else {
                                                echo "\tInstance " . $instance['InstanceId'] . " - not stopped\n";
                                        }
                                } else {
                                        if ($instance['State']['Code'] == 16) {
                                                array_push($instanceIds, $instance['InstanceId']);
                                                echo "\tIdentified instance " . $instance['InstanceId'] . "\n";
                                        } else {
                                                echo "\tInstance " . $instance['InstanceId'] . " - already stopped\n";
                                        }
                                }
                        }
                }
        }

        if ($start) {
                if (count($instanceIds) > 0) {
                        echo "\n\tStarting identified instances in " . $region . "...\n";
                        $ec2Current->startInstances(array(
                                "InstanceIds" => $instanceIds
                        ));
                } else {
                        echo "\n\tNo instances to start in " . $region . "\n";
                }
        } else {
                # Stop all identified instances.
                if (count($instanceIds) > 0) {
                        echo "\n\tStopping identified instances in " . $region . "...\n";
                        $ec2Current->stopInstances(array(
                                "InstanceIds" => $instanceIds
                        ));
                } else {
                        echo "\n\tNo instances to stop in " . $region . ".\n";
                }
        }
}
?>

```

Run the stopinator script
```
./stopinator.php -t"Project=ERPSystem;Environment=development"
```
Output:
```
Region is eu-north-1

    No instances to stop in Array.
Region is ap-south-1

    No instances to stop in Array.
Region is eu-west-3

    No instances to stop in Array.
Region is eu-west-2

    No instances to stop in Array.
Region is eu-west-1

    No instances to stop in Array.
Region is ap-northeast-2

    No instances to stop in Array.
Region is ap-northeast-1

    No instances to stop in Array.
Region is sa-east-1

    No instances to stop in Array.
Region is ca-central-1

    No instances to stop in Array.
Region is ap-southeast-1

    No instances to stop in Array.
Region is ap-southeast-2

    No instances to stop in Array.
Region is eu-central-1

    No instances to stop in Array.
Region is us-east-1

    No instances to stop in Array.
Region is us-east-2

    No instances to stop in Array.
Region is us-west-1

    No instances to stop in Array.
Region is us-west-2
    Identified instance i-06df1095f11449242
    Identified instance i-0e68e24a958567ba9

    Stopping identified instances in Array...

```
Restart Instances with stopinator
```
./stopinator.php -t"Project=ERPSystem;Environment=development" -s
```

##### Task 3: Terminate Non Compliant Instances

```
vi terminate-instances.php
```
Contents:
```
#!/usr/bin/php

<?php
require 'vendor/autoload.php';
use Aws\Ec2\Ec2Client;

$region = "us-west-2";
$subnetid = "";
$profile = "default"; # Only needed if not using IAM roles

# Necessary to quell a PHP error.
date_default_timezone_set('America/Los_Angeles');

array_shift($argv);
if (count($argv>0)) {
        do {
                $elem = array_shift($argv);
                if ($elem == "-region") {
                        $region = array_shift($argv);
                } elseif ($elem == "-subnetid") {
                        $subnetid = array_shift($argv);
                }
        } while (count($argv) > 0);
}

# Iterate through all available AWS regions.
$ec2 = Ec2Client::factory(array(
        'profile' =>$profile,
        'region' => $region
));

# Obtain a list of all instances with the Environment tag set.
$goodInstances = array();
$terminateInstances = array();

$tagArgs = array();
array_push($tagArgs,  array(
        'Name' => 'tag-key',
        'Values' => array('Environment')
));
$ec2DescribeArgs['Filters'] = $tagArgs;

$result = $ec2->describeInstances($ec2DescribeArgs);
foreach ($result['Reservations'] as $reservation) {
        foreach ($reservation['Instances'] as $instance) {
                $goodInstances[$instance['InstanceId']] = 1;
        }
}

# Obtain a list of all instances.
$subnetArgs = array();
array_push($subnetArgs, array(
        'Name' => 'subnet-id',
        'Values' => array($subnetid)
));
$ec2DescribeArgs['Filters'] = $subnetArgs;

$result = $ec2->describeInstances($ec2DescribeArgs);
foreach ($result['Reservations'] as $reservation) {
        foreach ($reservation['Instances'] as $instance) {
                echo "Checking " . $instance['InstanceId'] . "\n";
                if (!array_key_exists($instance['InstanceId'], $goodInstances)) {
                        $terminateInstances[$instance['InstanceId']] = 1;
                }
        }
}

# Terminate all identified instances.
if (count($terminateInstances) > 0) {
        echo "Terminating instances...\n";
        $ec2->terminateInstances(array(
                "InstanceIds" => array_keys($terminateInstances),
                "Force" => true
        ));
        echo "Instances terminated.\n";
} else {
        echo "No instances to terminate.\n";
}
?>

```

Go to EC2 Console >> Select an Instacne >> Tags >> add/edit tags 

Find the Environment tag >> Remove >> Save

Get AZ (all but last letter) <- Will be referred to as 'Region': us-west-2 
Get Subnet ID: subnet-0ce63c123f3f86d45

Run Command
```
./terminate-instances.php -region us-west-2 -subnetid subnet-0ce63c123f3f86d45
```
Output 
```
Checking i-0a026b62b779c8fb3
Checking i-0c60492a323ece9d8
Checking i-06df1095f11449242
Checking i-04785d12a5b6b9a46
Checking i-06c6f7163181fb813
Checking i-08ea4ab4ea110de41
Checking i-0587dc1510cb657ee
Checking i-0e25b377f67b05b02
Checking i-0e68e24a958567ba9
Checking i-0e7c1dd51456bc24c
Terminating instances...
Instances terminated.
```
